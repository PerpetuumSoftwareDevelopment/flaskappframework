# Flask application object creation

from flask import Flask
from flask_bootstrap import Bootstrap
#- from flask_sqlalchemy import SQLAlchemy
#- from flask_redis import  FlaskRedis

# Globally accessible libraries - create instances
bootstrap = Bootstrap()
#- db = SQLAlchemy()
#- r = FlaskRedis()

def create_app():
    # Initialize the core application. - configured using class Config in config.py
    app = Flask(__name__, instance_relative_config=False)
    
    # configure app (comment out as required)
    # # - use a production configuration
    # app.config.from_object('config.ProdConfig')
    # - use a development configuration
    app.config.from_object('config.DevConfig')

    # Initialize Plugins
    bootstrap.init_app(app)
    #- db.init_app(app)
    #- r.init_app(app)

    # Any part of the app not imported, registered or initialised inside the 'with app.app.context(): effectivlaey doesn't exist
    with app.app_context():
        # Include our Routes
        from . import routes

        # Register Blueprints
        #- app.register_blueprint(auth.auth_bp)
        #- app.register_blueprint(admin.admin_bp)

        return app