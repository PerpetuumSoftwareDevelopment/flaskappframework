# config.py
# contains application configuration setup.
# Flask variables set in '.flaskenv' file.
# Application variables set in '.env' file.

from os import environ, path
from dotenv import load_dotenv  ##### INSTALL python-dotenv ######

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

# Base configuration
class Config:
    # get application environmental variables from .env file, or use default
    SECRET_KEY = environ.get('SECRET_KEY') or 'you-will-never-guess'
#    SESSION_COOKIE_NAME = environ.get('SESSION_COOKIE_NAME')

    # set other application configuration variables
    STATIC_FOLDER = 'static'
    TEMPLATES_FOLDER = 'templates'

# Production environment configuration
class ProdConfig(Config):
    # FLASK_ENV should be seet in '.flaskenv' file. Set here to ease alternate configuration selection
    FLASK_ENV = 'production'
    DEBUG = False
    TESTING = False
#    DATABASE_URI = os.environ.get('PROD_DATABASE_URI')

# Development environment configuration
class DevConfig(Config):
    # FLASK_ENV should be seet in '.flaskenv' file. Set here to ease alternate configuration selection
    FLASK_ENV = 'development'
    DEBUG = True
    TESTING = True
#    DATABASE_URI = os.environ.get('DEV_DATABASE_URI')


