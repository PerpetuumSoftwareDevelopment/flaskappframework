The Flask Application Factory pattern

/app
├── /application
│   ├── __init__.py             - application initialisation
│   ├── forms.py                - flask forms logic
│   ├── ? models.py               - database models
│   ├── routes.py               - view functions - handlers for application routes
│   ├── /static
│   │   ├── /css                - style sheets
│   │   |   └── home.css        - home base layout style sheet (example)
│   │   └── web-logo.png        - perpetuum logo
│   └── /templates
│       |── home.html           - home / index page (example)
│       └── layout.html         - base page layout (example)
├── config.py                   - application configuration classes
├── .env                        - application environmental variables
├── .flaskenv                   - Flask CLI environmental variables
├── ReadMe.txt                  - this file
├── requirements.txt            - project requirements
└── wsgi.py                     - Web Server Gateway Interface - run file for application
? = optional files

Notice there's no app.py, main.py, or anything of the sort in the base directory. 
Instead, the entirety of the app lives in the /application folder, with the creation of the app happening in __init__.py. 
The init file is where we actually create what's called the Application Factory.

If you're wondering how we deploy an app where the main entry point isn't in the root directory. 
As the app is being created in application/__init__.py, so a file called wsgi.py simply imports this file to serve as our app gateway.


Application run hierarchy

wsgi.py
    __init__.py ->
            config.py ->
                .flaskenv
                .env
            imports routes.py
